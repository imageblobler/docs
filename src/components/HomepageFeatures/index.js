import React from 'react';
import clsx from 'clsx';
import styles from './styles.module.css';

const FeatureList = [
  {
    title: 'Лекго в использовании',
    Svg: require('@site/static/img/easy.svg').default,
    description: (
      <>
        ImageBlobler разработан таким образом, чтобы его использование приносило удовольствие,
        а интерфейс был интуитивно понятен
      </>
    ),
  },
  {
    title: 'Сосредоточься на анализе',
    Svg: require('@site/static/img/analize.svg').default,
    description: (
      <>
        ImageBlobler берет на себя рутинную работу по обработке данных, позволяя ученым сосредоточиться
        на анализе результата
      </>
    ),
  },
  {
    title: 'Работает быстро',
    Svg: require('@site/static/img/fast.svg').default,
    description: (
      <>
        В основе приложения лежат высокопроизводительные технологии для Python такие как numpy,
        FastAPI и sklearn
      </>
    ),
  },
];

function Feature({Svg, title, description}) {
  return (
    <div className={clsx('col col--4')}>
      <div className="text--center">
        <Svg className={styles.featureSvg} role="img" />
      </div>
      <div className="text--center padding-horiz--md">
        <h3>{title}</h3>
        <p>{description}</p>
      </div>
    </div>
  );
}

export default function HomepageFeatures() {
  return (
    <section className={styles.features}>
      <div className="container">
        <div className="row">
          {FeatureList.map((props, idx) => (
            <Feature key={idx} {...props} />
          ))}
        </div>
      </div>
    </section>
  );
}
