import api from '.';

export function getHello() {
    return api.get(`/hello`);
}

export function sendFile(data : FormData) {
    return api.post(`/sendFile`, data);
}

export function sendFileDebug(data : FormData) {
    return api.post('./sendFileDebug', data);
}

export function processImage(data: FormData) {
    return api.post('/processImage', data);
}

export function simulateAlgorithm(data: FormData) {
    return api.post('/simulateAlgorithm', data);
}

export function getAlgorithmBlocks() {
    return api.get('/getAlgorithmBlocks');
}

export function getDefaultAlgorithms() {
    return api.get('/getDefaultAlgorithms');
}