import axios from 'axios';

export default axios.create({
  //baseURL: `http://jsonplaceholder.typicode.com/`
  baseURL: `${window.location.protocol}//localhost:8000/`,
});