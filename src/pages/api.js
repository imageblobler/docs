import React from 'react';
import Layout from '@theme/Layout';
import spec from './openapi.json';
import Iframe from 'react-iframe'
import SwaggerUI from "swagger-ui-react"
import "swagger-ui-react/swagger-ui.css"
import './api.css'

export default function Api() {
  return (
    <Layout title="API" description="Hello React Page">
      <div style={{padding: "2rem"}}>
        {/* <Iframe url="http://127.0.0.1:8000/docs"
          width="100%"
          height="100%"
          id=""
          className=""
          display="absolute"/> */}
        <SwaggerUI url="http://127.0.0.1:8000/openapi.json" />
      </div>
    </Layout>
  );
}