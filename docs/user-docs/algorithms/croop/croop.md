---
sidebar_position: 2
---

# Обрезка изображения

## Описание

Часто необоходимо обрабатывать не всё изображение целиком, а только его часть. В ImageBlobler
существует инструмент для обрезки изображения.

## Панель инструмента

Если ваш алгоритм содержит блок для обрезания изображения, перейдите в специальную панель для выбора рамки:

![Панель инструмента](./img/croop-mode.png)

## Пример

После применения данного блока обработки, останется только та часть изображения, которая попала в выбранную рамку:

![Пример](./img/croop-result.png)

Следующие блоки обработки будут работать с обрезанным изображением.

## Параметры

Обрезка изображения задается 4 параметрами:

| Название параметра        | Описание           | Диапазон значений  |
| ------------- |:-------------| :-----:|
| X  | Значение координаты по оси X верхней левой вершины прямоугольной области в пикселях | 0 - ширина изорбражения |
| Y  | Значение координаты по оси Y верхней левой вершины прямоугольной области в пикселях | 0 - высота изорбражения |
| Ширина    | Значение ширины прямоугольной области в пикселях  | 0 - ширина изорбражения |
| Высота    | Значение высоты прямоугольной области в пикселях  | 0 - высота изорбражения |

## Функиональный блок

Блок обрезки изображения относится к блокам обработки, нажмите на "Croop" для добавления его в алгоритм. 

![Блок обрезки изображения](./img/croop-block.png)

:::tip

Обычно данный блок находится в начале алгоритма, а последующие блоки работают с обрезанным изображением. Это значительно ускоряет процесс обработки.

:::