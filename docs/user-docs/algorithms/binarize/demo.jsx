import React, {useState, useEffect} from "react";
import {simulateAlgorithm} from '../../../../src/components/api/requests';
import {base64toBlob} from '../../utils/ProcessUtils';
import example from './img/example.jpeg';

const GetAlgorithm = (value) => {
    return {"blocks":[
    {"type":"Preprocess",
    "name":"Binarize","description":"Binarize an image",
    "parameters":[{"type":{"py/type":"builtins.int"},
    "name":"Thresh",
    "unit":"px",
    "default_value":50,
    "value":value}]}]}
}

const Demo = () => {

    const [originalFile, setOriginalFile] = useState(null);
    const [processedFile, setProcessedFile] = useState(null);
    const [serverError, setServerError] = useState(null);
    const [thresh, setThresh] = useState(50);

    useEffect(async () => {
        const file = await fetch(example)
        setOriginalFile(new File([new Uint8Array(await file.arrayBuffer())], "example"))
    }, [])

    const processFile = async () => {

        if (!originalFile) {return;}

        try {
            const algorithm = GetAlgorithm(Number(thresh));
            let blobData = new Blob([new Uint8Array(await originalFile.arrayBuffer())], { type: originalFile?.type });
            
            let formData = new FormData();
            formData.append("file", blobData);
            formData.append("algorithm", JSON.stringify(algorithm));
            
            simulateAlgorithm(formData).then((x) => {
                x.data.forEach((element) => {
                    let blobAfter = base64toBlob(element.image_after, "image/png");
                    let afterBefore = new File([blobAfter], "file_after", { type: 'image/png' });
                    setProcessedFile(afterBefore);
                });
            }).catch((e) => {
                setServerError(true);
                console.log(e);
            });
        } catch (e) {
            console.log(e);
        }
    }

    let fileToShow = example;

    try {
        fileToShow = processedFile ?  URL.createObjectURL(processedFile) : example;
    } catch (e) {
        console.log(e);
    }

    return <div className="input-group">
        <div className="margin-bottom--md">
            <label
                for="customRange1"
                class="mb-2 font-bold inline-block text-neutral-700 dark:text-neutral-200"
                >
                    {"Порог цвета: " + thresh} 
            </label>
            <input
                onChange={async (e) => {
                    setThresh(e.target.value);
                    try {
                        await processFile();
                    } catch (e) {
                        console.log(e);
                    }
                }}
                style={{appearance:"none"}}
                type="range"
                min="0"
                max="255"
                class="transparent h-[4px] w-full cursor-pointer appearance-none border-transparent bg-neutral-200 dark:bg-neutral-600"
                id="customRange1" />
        </div>
        <div className="input-group margin-bottom--md">
            <button className="button button--primary">
                <label className="btn btn-default btn-file image-menu-font">
                    Выбрать файл
                    <input hidden={true}
                        type="file"
                        name="files"
                        id="ctrl"
                        onChange={async (event) => {
                            if (event != null && event.target != null) {
                                setOriginalFile(event.target.files[0]);
                                await processFile();
                            }
                        }}
                    />
                </label>
            </button>
        </div>
        <div>
            <img  
            src={fileToShow}
            alt="Обработанный файл"/>
        </div>
    </div>
}

export default Demo;