---
sidebar_position: 1
---

# Введение

ImageBlobler обрабатывает изображения согласно некоторому алгоритму. Существуют готовые алгоритмы, но 
можно сконструировать собственный, используя специальные функциональные блоки

## Выбор алгоритма

Алгоритм можно выбрать используя выпадающий список на боковой панель справа

![Выбор алгоритма](./img/alg-choose.png)

Либо перейдя в настройки, в разделе "Алгоритмы"

![Выбор алгоритма 2](./img/alg-choose-2.png)

## Функциональные блоки

Функциональные блоки делятся на 2 типа: **Обработка** и **Результат**

### Обработка

Блоки обработки приводят изображение к виду, из которого можно легко получить ту или иную характеристику, к ним относятся:

- Обрезка изображения
- [Бинаризация](/docs/algorithms/binarize)
- Оператор Собеля
- Чёрно-белый фильтр
- Кластеризация DBSCAN
- Кластеризация KMeans
- Кластеризация KMeans++

### Результат

Блоки результата извлекают некоторую характеристику из обработанного изображения, к ним относятся:

- Получение высоты
- Получение ширины
- Получение площади соприкосновения с поверностью
